﻿using Orchard;
using Orchard.Themes;
using Orchard.Themes.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dolph.AdminViewEditor.Controllers
{
    public class FileViewerViewModel
    {
        public string[] Files {get;set;}
    }

    public class AdminController : Controller
    {
        private readonly ISiteThemeService _siteThemeService;

        public AdminController(ISiteThemeService siteThemeService)
        {
            _siteThemeService = siteThemeService;
        }

        public ActionResult Index()
        {
            var themePath = _siteThemeService.GetSiteTheme().Path;

            var themeViewsPath = Server.MapPath(string.Format("~/Themes/{0}/Views", themePath));
            
            //string[] fileEntries = Directory.GetFiles(themeViewsPath).Select(x => x.Replace(themeViewsPath, string.Empty)).ToArray();

            var viewModel = new FileViewerViewModel() { Files = GetAllFiles(themeViewsPath).Select(x => x.Replace(themeViewsPath, string.Empty)).ToArray() };

            return View(viewModel);
        }

        public static IEnumerable<string> GetAllFiles(string path, Func<FileInfo, bool> checkFile = null)
        {            
            var mask = "*.cshtml";
            path = Path.GetDirectoryName(path);
            string[] files = Directory.GetFiles(path, mask, SearchOption.AllDirectories);
            foreach (string file in files)
            {
                if (checkFile == null || checkFile(new FileInfo(file)))
                    yield return file;
            }
        }

        [Themed(false)]
        public ActionResult GetFile(string fileName)
        {
            var themePath = _siteThemeService.GetSiteTheme().Path;

            var themeViewsPath = Server.MapPath(string.Format("~/Themes/{0}/Views", themePath));

            var fileContents = string.Empty;
            using (StreamReader sr = new StreamReader(themeViewsPath + fileName))
            {
                fileContents = sr.ReadToEnd();
            }

            TempData["Razor"] = fileContents;

            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult SaveFile(string fileName, string content)
        {
            var themePath = _siteThemeService.GetSiteTheme().Path;

            var themeViewsPath = Server.MapPath(string.Format("~/Themes/{0}/Views", themePath));

            using (var file = new System.IO.StreamWriter(themeViewsPath + fileName))
            {
                file.Write(@content);
            }

            return Json(true);
        }
    }
}