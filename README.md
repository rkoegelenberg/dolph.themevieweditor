# Dolph.ThemeViewEditor #

An Orchard CMS Module which allow to edit the current theme's views in the Admin area.

WARNING: Only for experienced users.

## Installation Instructions ##

Enable Dolph.ThemeViewEditor module in admin, this will add a Theme View Editor menu item to the Admin menu.

## How to use it ##

Once enabled, click on link. It will list all the files list in your theme's Views directory.  Click on the file you wish to edit, it show the content of the file. Make the necessary changes and click **Save** once you're happy with the changes.
