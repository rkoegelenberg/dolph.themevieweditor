using Orchard.UI.Resources;

namespace Dolph.AdminViewEditor {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();

            manifest
                .DefineScript("AdminViewEditor")
                .SetUrl("Admin-View-Editor.js")
                .SetDependencies("jQuery");
           
        }
    }
}
