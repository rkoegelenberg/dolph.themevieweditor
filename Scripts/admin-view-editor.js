﻿(function ($) {
    var files = $('.ave-file'),
        contentTextArea = $('.file-contents'),
        filename = $('.filename'),
        saveTemplate = $('.save-template'),
        antiftoken = $('#antiforgerytoken'),
        fileEditor = $('.file-editor');

    fileEditor.hide();

    files.on('click', function (e) {
        e.preventDefault();        
        var href = $(this).attr('href');
        var selectedFile = href.split('fileName=')[1];
        $.get(href, function (result) {
            filename.text(selectedFile);
            contentTextArea.html(result);
            $('.file-editor').slideDown();
        });        
    });

    saveTemplate.on('click', function (e) {
        e.preventDefault();
        var currentFileName = filename.text();
        $.post('/Admin/Dolph.AdminViewEditor/SaveFile', 'fileName=' + currentFileName + '&content=' + encodeURIComponent(contentTextArea.val()) + '&__RequestVerificationToken=' + antiftoken.val(), function (result) {
            $('#messages').remove();
            $('#main').prepend('<div id="messages"><div class="zone zone-messages"><div class="message message-Information">Your ' + currentFileName + ' has been saved.</div></div></div>');
        });
    });
})(jQuery);